//
//  UILabelExts.swift
//  demoAutoLayout
//
//  Created by PhuocNguyen on 11/14/18.
//  Copyright © 2018 PhuocNguyen. All rights reserved.
//

import Foundation
import UIKit

extension UILabel {
    func fomartMainTilte(size: CGFloat){
        guard let customFont = UIFont(name: "Roboto-Regular", size: size) else {
            fatalError("""
        Failed to load the "CustomFont-Light" font.
        Make sure the font file is included in the project and the font name is spelled correctly.
        """)
        }
        self.font = UIFontMetrics.default.scaledFont(for: customFont)
        self.adjustsFontForContentSizeCategory = true
    }
}
