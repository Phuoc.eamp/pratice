//
//  ViewController.swift
//  demoAutoLayout
//
//  Created by PhuocNguyen on 11/14/18.
//  Copyright © 2018 PhuocNguyen. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    let btnColor: UIColor = UIColor(red: 10/255, green: 50/255, blue: 200/255, alpha: 1)

    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var loginLb: UILabel!
    
    @IBOutlet weak var midView: UIView!
    @IBOutlet weak var userLb: UILabel!
    
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var userTextField: UITextField!
    @IBOutlet weak var btnLogin: UIButton!
    
   
    @IBOutlet weak var passwordLb: UILabel!
    
    @IBOutlet weak var bottView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        setupView()
    }

    func setupView() {
        view.addSubview(topView)
        setupTopView()
        
        view.addSubview(midView)
        setupMidView()
        
        view.addSubview(bottView)
        setupBottView()
    }
    
    func setupTopView() {
        
        topView.translatesAutoresizingMaskIntoConstraints = false
        topView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
        topView.heightAnchor.constraint(equalTo: view.heightAnchor, multiplier: 0.3).isActive = true
        topView.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 1).isActive = true
        
        topView.addSubview(loginLb)
        loginLb.translatesAutoresizingMaskIntoConstraints = false
        loginLb.centerXAnchor.constraint(equalTo: topView.centerXAnchor).isActive = true
        loginLb.centerYAnchor.constraint(equalTo: topView.centerYAnchor).isActive = true
        loginLb.text = "LOGIN"
        
        
        loginLb.fomartMainTilte(size: 35)
        
    }
    
    func setupMidView() {
        
        midView.translatesAutoresizingMaskIntoConstraints = false
        midView.heightAnchor.constraint(equalTo: view.heightAnchor, multiplier: 0.5).isActive = true
        midView.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.6).isActive = true
        midView.topAnchor.constraint(equalTo: topView.bottomAnchor).isActive = true
        midView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        //midView.backgroundColor = UIColor.gray
        
        
        midView.addSubview(userLb)
        setupMidViewLabel(label: userLb)
        userLb.text = "Username"
        userLb.topAnchor.constraint(equalTo: midView.topAnchor)
        midView.addSubview(userTextField)
        setupMidViewTextField(textField: userTextField)
        userTextField.topAnchor.constraint(equalTo: userLb.bottomAnchor, constant: 10).isActive = true
        
        midView.addSubview(passwordLb)
        setupMidViewLabel(label: passwordLb)
        passwordLb.text = "Password"
        passwordLb.topAnchor.constraint(equalTo: userTextField.bottomAnchor, constant: 10).isActive = true
        midView.addSubview(passwordTextField)
        setupMidViewTextField(textField: passwordTextField)
        passwordTextField.topAnchor.constraint(equalTo: passwordLb.bottomAnchor, constant: 10).isActive = true

       
        
    }
    
    func setupMidViewLabel(label: UILabel) {
        label.translatesAutoresizingMaskIntoConstraints = false
        label.leftAnchor.constraint(equalTo: midView.leftAnchor).isActive = true
        
        label.fomartMainTilte(size: 20)
    }

    func setupMidViewTextField(textField: UITextField) {
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.leftAnchor.constraint(equalTo: midView.leftAnchor).isActive = true
        textField.widthAnchor.constraint(equalTo: midView.widthAnchor, multiplier: 1).isActive = true
    }
    
    func setupBottView() {
        bottView.translatesAutoresizingMaskIntoConstraints = false
//        bottView.heightAnchor.constraint(lessThanOrEqualTo: view.heightAnchor, multiplier: 0.2).isActive = true
        bottView.topAnchor.constraint(equalTo: midView.bottomAnchor).isActive = true
        bottView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor).isActive = true
        bottView.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 1).isActive = true
        //bottView.layer.cornerRadius = 10
        //bottView.backgroundColor = UIColor.black
        
        bottView.addSubview(btnLogin)
        btnLogin.translatesAutoresizingMaskIntoConstraints = false
        btnLogin.centerXAnchor.constraint(equalTo: bottView.centerXAnchor).isActive = true
        btnLogin.centerYAnchor.constraint(equalTo: bottView.centerYAnchor).isActive = true
        btnLogin.setTitle("Login", for: .normal)
        btnLogin.titleLabel?.font = UIFont.boldSystemFont(ofSize: 35)
        btnLogin.widthAnchor.constraint(equalTo: bottView.widthAnchor, multiplier: 0.5).isActive = true
        btnLogin.heightAnchor.constraint(equalTo: bottView.heightAnchor, multiplier: 1).isActive = true

        btnLogin.layer.backgroundColor = btnColor.cgColor
        btnLogin.setTitleColor(UIColor.white, for: .normal)
        btnLogin.layer.cornerRadius = 10
        
    }
}

