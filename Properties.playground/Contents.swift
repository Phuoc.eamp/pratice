//: Playground - noun: a place where people can play

import UIKit

// lazy properties
struct Student{
    lazy var age: Int = 27
    var name: String = "aaaa"
}

var he = Student()
print(he)
he.age
print(he)
he.age = 28
print(he)

struct Circle {
    lazy var pi = {
        return ((4.0 * atan(1.0 / 5.0)) - atan(1.0 / 239.0)) * 4.0
    }()
    var radius : Double
    var circumference: Double {
        mutating get {
            return pi * radius * 2
        }
        set {
            radius = newValue/(2*pi)
        }
    }
    var read: String{
        return "123"
    }
    var totalSteps: Int = 0 {
        willSet(newTotalSteps) {
            print("About to set totalSteps to \(newTotalSteps)")
        }
        didSet {
            if totalSteps > oldValue  {
                print("Added \(totalSteps - oldValue) steps")
            }
        }
    }
    
    init (radius: Double) {
        self.radius = radius
    }
}
var circle = Circle(radius: 5)
print(circle)
circle.circumference = 34
circle.pi



//
struct IceCream {
    var name: String
    lazy var ingredients : [String] = []
    
    init(name: String) {
        self.name = name
        
    }
}

var ice = IceCream(name: "abc")
print(ice)
ice.ingredients.append("Ice")
ice.ingredients.append("Banana")
print(ice.ingredients)

//
struct FuelTank {
    var totalFuel: Double
    var fuel: Double // min (0.5) max(5.0)
    var level: Double {
        get{
            if fuel <= 0.5{
                return 0.0
            }else{
                return fuel/totalFuel
            }
        }
    }
    var lowFuel: Bool {
        if level <= 0.1 {
            print("Low fuel. Please fill!!")
            return true
        }else{
            return false
        }
        
    }
    
    init(fuel: Double,totalFuel: Double) {
        self.fuel = fuel
        self.totalFuel = totalFuel
        print("Fuel tank: \(level)")
//        if level > 0.1{
//            lowFuel = false
//        }else{
//            lowFuel = true
//        }
    }
}
var motobike = FuelTank(fuel: 3.5, totalFuel: 5.0)
motobike.lowFuel






