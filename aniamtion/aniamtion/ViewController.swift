//
//  ViewController.swift
//  aniamtion
//
//  Created by PhuocNguyen on 11/21/18.
//  Copyright © 2018 PhuocNguyen. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        //setupViewAnimation()
        
    }
    
    func setupShadowBorderImage() {
        let shadowView = UIView()
        shadowView.frame = CGRect(x: 100, y: 100, width: 200, height: 200)
        shadowView.backgroundColor = UIColor.clear
        shadowView.layer.shadowColor = UIColor.black.cgColor
        shadowView.layer.shadowOffset = CGSize(width: 3, height: 3)
        shadowView.layer.shadowOpacity = 0.7
        shadowView.layer.shadowRadius = 4.0
        let borderView = UIView()
        borderView.frame = shadowView.bounds
        borderView.layer.cornerRadius = 10
        borderView.layer.borderColor = UIColor.black.cgColor
        borderView.layer.borderWidth = 1.0
        borderView.layer.masksToBounds = true
        shadowView.addSubview(borderView)
        let otherSubContent = UIImageView()
        otherSubContent.image = UIImage(named: "hinh2.jpg")
        otherSubContent.frame = borderView.bounds
        borderView.addSubview(otherSubContent)
        view.addSubview(shadowView)
    }
    
    func setupMask() {
        let redView = UIView(frame: CGRect(x: 50, y: 50, width: 128, height: 128))
        redView.backgroundColor = .red
        view.addSubview(redView)
        let maskView = UIView(frame: CGRect(x: 64, y: 0, width: 128, height: 128))
        maskView.backgroundColor = .blue
        maskView.layer.cornerRadius = 64
        redView.mask = maskView
    }
    
    func setupImageAnimation() {
        let image = UIImageView()
        image.frame = CGRect(x: 150, y: 300, width: 200, height: 150)
        image.animationImages = [UIImage(named: "hinh1")!,
                                        UIImage(named: "hinh2")!
                                        ]
        image.animationDuration = 1
        image.animationRepeatCount = 0
        image.startAnimating()
        view.addSubview(image)
    }
    
    func setupViewAnimation() {
        let image = UIImageView()
        image.frame = CGRect(x: 150, y: 300, width: 200, height: 150)
        image.image = UIImage(named: "hinh1")
        view.addSubview(image)
        UIView.animate(withDuration: 2,
                       delay: 0,
                       //usingSpringWithDamping: 0.4,
                       //initialSpringVelocity: 0.7,
                       options: [.repeat, .autoreverse, .curveEaseInOut],
                       animations: {
                            image.transform = CGAffineTransform(rotationAngle: 45)
                            image.frame = CGRect(x: 50, y: 300, width: 200, height: 150)
                            image.transform = CGAffineTransform(scaleX: 2, y: 2)
                        }) { (_) in}
    }
    
}

