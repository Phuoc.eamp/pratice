//
//  imageExts.swift
//  paw
//
//  Created by PhuocNguyen on 11/18/18.
//  Copyright © 2018 PhuocNguyen. All rights reserved.
//

import Foundation
import UIKit

class BaseImageView: UIImageView{
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.layer.cornerRadius = self.bounds.height/2
    }
    
//    func cycleImage(image: UIImageView) {
//        
//    }
}
