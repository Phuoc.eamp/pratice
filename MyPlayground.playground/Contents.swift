//: Playground - noun: a place where people can play

import UIKit
import Foundation

var str = "Hello, playground"
let s = "hello"
let range = (s as NSString).range(of:"ell")
let a = s.range(of:"ell")
let s2 = "world"
let space = " "
let greeting = [s,s2].joined(separator:space)

var c: NSString = "a"
c = "b"
print(c)

let number1 = NSDecimalNumber(value: 3.2)
let number2 = NSDecimalNumber(value: 5.0)
number1.adding(number2)
let dec1 = Decimal(4.2)
let dec2 = Decimal(5.0)
let sum = dec1 + dec2

var ab: NSString = "ab"
var cd = ab

var i: Int = 8
var n = NSNumber(value: i)
UserDefaults.standard.set(1, forKey:"Score")

let date1 = Date()
let date = NSDate(timeInterval: 300, since: date1)
date.earlierDate(date1)
date.addingTimeInterval(3000)


let dateFormatter = DateFormatter()
dateFormatter.dateFormat = "EEE, MM/dd/yyyy HH:mm:ss zzz"
print(dateFormatter.string(from: date1))


let dateString = "Sun, 10 Nov 2018, 10:20:15 GMT+7"
dateFormatter.dateFormat = "EEE, dd MMM yyyy, HH:mm:ss zzz"
print(dateFormatter.date(from: dateString))

//let calender = NSCalendar.current
////let dateComponents = calendar.components([NSCalendarUnit.Day, NSCalendarUnit.Month, NSCalendarUnit.Year, NSCalendarUnit.WeekOfYear, NSCalendarUnit.Hour, NSCalendarUnit.Minute, NSCalendarUnit.Second, NSCalendarUnit.Nanosecond], fromDate: date1)
//let dateComponents = calender.dateComponents(Set<NSCalendar.Unit.year>, from: date1)

