//: Playground - noun: a place where people can play

import UIKit

enum Optional<T> {
    case none
    case some(T)
    var value: T? {
        switch self {
        case .none:
            print("No have value")
            return nil
        case .some(let val):
            print(val)
            return val
        }
    }
    
}
var mess: String? = "Hello"
Optional<String>.some(mess!).value
enum Unwrapper<Wrapper> {
    case none
    case some(Wrapper?)
    var check: Wrapper? {
        switch self {
        case .none:
            return nil
        case .some(let val):
            return val != nil ? val: nil
        }
    }
}
mess = "hello"
Unwrapper<String>.some(mess).check

protocol Job {}
protocol Fed: Job {}
extension Fed {
    func fed() {
        print("Do fed")
    }
}
//Add obj have protocol type to array have able job
protocol Tanks: Job {}
extension Tanks {
    func tanks() {
        print("Have tanks")
    }
}
//
protocol Clean: Job {}
extension Clean {
    func clean() {
        print("Clean")
    }
}
//
protocol Caged: Job {}
extension Caged {
    func caged() {
        print("Have caged")
    }
}
//
protocol Exercise: Job {}
extension Exercise {
    func exr() {
        print("Do exercise")
    }
}

class Cat: Fed,Exercise,Clean {}
class Fish: Fed,Tanks,Clean {}
class Bird: Fed,Caged,Clean {}
class Lobster: Fed,Tanks,Clean {}
class Dog: Fed,Exercise,Clean {}
class Bear: Fed,Tanks,Exercise,Clean{}
var a = Dog()
var b = Cat()
var c = Fish()
var d = Lobster()
var e = Bird()
var f = Cat()
var g = Bear()
var caged: [Caged] = []
var fed: [Fed] = []
var clean: [Clean] = []
var tank: [Tanks] = []
var exercise: [Exercise] = []
var arr: [Job] = [a,b,c,d,e,f,g]
print(arr.map {$0 as? Dog})
//arr.forEach { (animal: Any) in
//    if let val = animal as? Fed {
//        fed.append(val)
//    }
//    if let val = animal as? Caged {
//        caged.append(val)
//    }
//    if let val = animal as? Clean {
//        clean.append(val)
//    }
//    if let val = animal as? Tanks {
//        tank.append(val)
//    }
//    if let val = animal as? Exercise {
//        exercise.append(val)
//    }
//
//}
//fed.append()
print(fed)


//add
//fed += arr.filter {$0 is Fed}.map {$0 as! Fed}
arr.filter {$0 is Fed}.map {fed.append($0 as! Fed)}
//arr.filter {$0 is Caged}.map {caged.append($0 as! Caged)}
caged += arr.map {$0 as? Caged}.compactMap {$0}

arr.filter {$0 is Tanks}.map {tank.append($0 as! Tanks)}
arr.filter {$0 is Clean}.map {clean.append($0 as! Clean)}
arr.filter {$0 is Exercise}.map {exercise.append($0 as! Exercise)}
print(fed)
print(caged)
print(tank)
print(clean)
print(exercise)
print(arr.filter {$0 is Tanks & Exercise})


func choice<T> (animals: [Job],to job: inout[T]) -> [T] {
    animals.filter {$0 is T}.map {job.append($0 as! T)}
    return job
}
var ca: [Caged] = []
choice(animals: arr, to: &ca)

extension Array where Element == Int {
    subscript (x: Element, y: Element) -> Double {
        return sqrt(Double(x*x + y*y))
    }
}

var point = [2]
point[1,1]



//use subcript
extension String {
    subscript (x: String, y: String) -> String {
        return x + " " + y
    }
}
extension Int {
    subscript (x: Int, y: Int) -> Double {
        return sqrt(Double(x*x + y*y))
    }
}

var str: String = ""
str ["hello","myfriend"]









