//: Playground - noun: a place where people can play

import UIKit

var numbers = [Int]()
func escapse(number: Int){
    numbers.append(number)
}
escapse(number: 3)
escapse(number: 4)
var completionHandlers: [() -> Void] = []
func someFunctionWithEscapingClosure(completionHandler: @escaping () -> Void) {
    completionHandlers.append(completionHandler)
}
func someFunctionWithNonescapingClosure(closure: () -> Void) {
    closure()
}

class SomeClass {
    var x = 10
    func doSomething() {
        someFunctionWithEscapingClosure { self.x = 100 }
        someFunctionWithNonescapingClosure { self.x = 200 }
    }
}

let instance = SomeClass()
instance.doSomething()
print(instance.x)
completionHandlers.first?()
print(instance.x)
class Address{
    var address: String
    weak var student: Student?
    init(address: String) {
        self.address = address
    }
    deinit {
        print("No address!!!")
    }
}
class Student{
    var address: Address?
    var name: String
    var age: Int
    lazy var introduce : () -> Int = { [unowned self] in
        return self.age
    }
//    lazy var introduce : () -> Int = { [unowned self] in
//        return self.age
//    }
    init(name: String, age: Int) {
        self.name = name
        self.age = age
    }
    deinit {
        print("Don't have any student!!!")
    }
}

var add : Address? = Address(address: "nguyen hoang street")
var john : Student? = Student(name: "John", age: 16)
add?.student = john
john?.address = add
john?.introduce()
//john = nil
//john?.introduce() //

class Person{
//    var student: Student?
    weak var student: Student?
    var job: String
    init(job: String) {
        self.job = job
    }
    deinit {
        print("No person!!!")
    }
}


var jane : Person? = Person(job: "student")
//jane = nil
print(john?.age)
jane?.student = john
john = nil

class Pet{
    var name: String
    init(name: String) {
        self.name = name
    }
    deinit {
        print("dealloc pet")
    }
}
var mypet : Pet? = Pet(name: "Lu")
weak var yourpet = mypet
mypet = nil

//
class User{
    var name: String
    var age: Int
    weak var phonenumber: PhoneNumber?
        
    
    init(name: String,age: Int) {
        self.name = name
        self.age = age
        print("""
            Name: \(name)
            Age: \(age)
            """)
    }
    deinit {
        print("Don't have any user")
    }
    
}
class PhoneNumber{
    unowned var user: User
    var number: Int
    
    init?(number: Int,user: User?){
        self.number = number
        guard let user  = user else {
            return nil
        }
        self.user = user
    }
    deinit {
        print("This user don't have phone number!!")
    }
}
var Army : User? = User(name: "Army", age: 18)


var phone : PhoneNumber? = PhoneNumber(number: 123456789, user: Army)
phone = nil
phone?.user
//Army?.phonenumber = phone
//var Army = User(name: "Army", age: 28)

class Number {
    var number: Int
    
    init?(number: Int) {
        guard number < 9 else {
            return nil
        }
        self.number = number
    }
    deinit {
        print("number is no exist!!")
    }
}
var number : Number? = Number(number: 8)
number = nil

