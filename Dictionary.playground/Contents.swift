//: Playground - noun: a place where people can play

import UIKit
// homeword
var dic: DictionaryLiteral = ["Anna": "1", "Brian": "3", "Craig": nil, "Donna": "4"]
var dict = dic.compactMap {$0.value}.map {$0 + "mp3"}
print(dict)
//print(dict.keys) // Call you tell me about this error
print(dict.first)


// swap values for keys
func swappingValuesForKeys(_ key1: String, _ key2: String,in dictionary: inout[String: Int]) -> [String: Int]{
    var swap : Int?
    swap = dictionary[key1]
    dictionary[key1] = dictionary[key2]
    dictionary[key2] = swap
    return dictionary
}
var dicA = ["first" : 1, "second" : 2]
swappingValuesForKeys("first", "second", in: &dicA)
print(dicA)


// print when value > 8, but you can tell me how to delete character " " in String
func printName(dictionary: [String: String]){
    
    for value in dictionary.values{
        if value.count > 8 {
            print(value)
        }
    }
}
var state = ["NY" : "New York","CA" : "California"]
print(state.values)
printName(dictionary: state)


// merging two dictionaries
func merging(_ dict1: inout[String: String?], with dict2: [String: String?]) ->[String: String?]{
    var _ : String?
        for (key,value) in dict2 {
            if value != nil {
                dict1[key] = value
            }
        }
    return dict1
}
var dict1 = ["a": "A", "b": nil, "c": "C"]
var dict2 = ["d": "D","c": nil, "e": "EE"]
print(merging(&dict1, with: dict2))
var a = merging(&dict1, with: dict2)
print(a)


// invertible
var string : String = "123"
var number : Int? = Int(string)
func isInvertible(_ dictionary: [String: Int]) -> Bool{
    for key in dictionary.keys{
        var a : Int? = Int(key)
        if a == nil{
            return false
        }
    }
    return true
}
var dictt = ["1": 1,"2": 2]
isInvertible(dictt)


// invert between keys and values of a dictionary
func invert(_ input: [String: Int]) -> [Int: String]{
    var output = [Int: String]()
    guard isInvertible(input) else {
        print( "this dictionary can't be inverted")
        return output
    }
    for (key,value) in input{
        output[value] = key
    }
    return output
}
invert(dictt)

var arr = ["a","b","c"]
var arrValue = [1,2,3]
var arrA = [String]()
var arrB = [Int]()
var dictA = [String:Int]()
var dictB = [Int:Int]()

arrValue.reduce(dictB) { (dic: [Int:Int], value: Int) -> [Int:Int] in
    var d = dic
    print(d)
    d[value] = value
    print(dictB)
    //dictB = d
    return d
}
arrValue.compactMap {$0}.map {dictA[String($0)] = $0}
arr.enumerated().forEach { (offset: Int,element: String ) in
    dictA[element] = arrValue[offset]
}
print(dictA)

//dictA.forEach {
//    arrA.append($0.key)
//    arrB.append($0.value)
//}
//
//print(arrA)
////print(arrB)
//print(dictB)
//var ab = "a b"


