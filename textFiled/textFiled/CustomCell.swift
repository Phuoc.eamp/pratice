//
//  CustomCell.swift
//  textFiled
//
//  Created by PhuocNguyen on 12/16/18.
//  Copyright © 2018 PhuocNguyen. All rights reserved.
//

import UIKit

class Cell: UITableViewCell {
    let title: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont(name: "Helvetica Neue", size: 17)
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.addSubview(title)
        title.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 10).isActive = true
        title.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
    
}

class BasicCell: Cell {
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    let detail: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Detail"
        label.font = UIFont(name: "Helvetica Neue", size: 17)
        return label
    }()
    
    func setupView(){
        self.addSubview(detail)
        detail.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -10).isActive = true
        detail.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
}

class SwitchCell: BasicCell{
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    let choose: UISwitch = {
        let choose = UISwitch()
        choose.translatesAutoresizingMaskIntoConstraints = false
        return choose
    }()
    
    override func setupView(){
        self.addSubview(choose)
        choose.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -10).isActive = true
        choose.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
    }
    
}

class ColorCell: Cell {
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    let colorTheme: UIButton = {
        let button = UIButton(type: .system)
        button.backgroundColor = UIColor(red: 26/255, green: 200/255, blue: 200/255, alpha: 1)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    func setupView(){
        self.addSubview(colorTheme)
        colorTheme.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -10).isActive = true
        colorTheme.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        colorTheme.heightAnchor.constraint(equalToConstant: 30).isActive = true
        colorTheme.widthAnchor.constraint(equalToConstant: 30).isActive = true
        colorTheme.clipsToBounds = true
        colorTheme.layer.cornerRadius = 15
    }
    
}

class DetailCell: BasicCell {
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func setupView() {
        //self.accessoryView?.tintColor =  UIColor(red: 26/255, green: 200/255, blue: 200/255, alpha: 1)
        //self.backgroundColor = UIColor(red: 26/255, green: 200/255, blue: 200/255, alpha: 1)
        //self.tintColor = UIColor(red: 26/255, green: 200/255, blue: 200/255, alpha: 1)
        self.accessoryType = .disclosureIndicator
        //self.accessoryView?.backgroundColor = UIColor.red
        self.addSubview(detail)
        detail.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -50).isActive = true
        detail.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
    }
}





