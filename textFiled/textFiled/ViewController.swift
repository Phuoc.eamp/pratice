//
//  ViewController.swift
//  textFiled
//
//  Created by PhuocNguyen on 12/12/18.
//  Copyright © 2018 PhuocNguyen. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate{
    @IBOutlet weak var buttonBtn: UIButton!
    let color: UIColor = (UIColor(red: 26/255, green: 200/255, blue: 200/255, alpha: 1))
    let line: DictionaryLiteral = ["Account": ["Signed In","Drive Detection"],
                "Subscription": ["Free","Get Unlimited Drives","Support Simple"],
                "Personalization": ["Vechiles","Custom Purposes","Mileage Rates","Theme","Language","Use Imperial Units(Miles)"],
                "Other Setting": ["Rate Made Simple","Version"]]
    //let detail = ["a@gmail.com","14 Drivers remaining","3","US","English","1.0"]
    @IBOutlet weak var MenuTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        MenuTableView.delegate = self
        MenuTableView.dataSource = self
        self.MenuTableView.register(BasicCell.self, forCellReuseIdentifier: "BasicCell")
        self.MenuTableView.register(DetailCell.self, forCellReuseIdentifier: "DetailCell")
        self.MenuTableView.register(ColorCell.self, forCellReuseIdentifier: "ColorCell")
        self.MenuTableView.register(SwitchCell.self, forCellReuseIdentifier: "SwitchCell")
        self.MenuTableView.register(CustomViewHeaderSection.self, forHeaderFooterViewReuseIdentifier: "HeaderSection")
        buttonBtn.setTitleColor(color, for: .normal)
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return line.count
    }
    
//    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
//        return line[section].key
//    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return line[section].value.count
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return section == 2 ? 70 : 30
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerSection = tableView.dequeueReusableHeaderFooterView(withIdentifier: "HeaderSection") as! CustomViewHeaderSection
        headerSection.label.text = line[section].key
        return headerSection
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if (cell.responds(to: #selector(getter: UIView.tintColor))){
            if tableView == self.MenuTableView {
                let cornerRadius: CGFloat = 12.0
                cell.backgroundColor = .clear
                let layer: CAShapeLayer = CAShapeLayer()
                let path: CGMutablePath = CGMutablePath()
                let bounds: CGRect = cell.bounds
                bounds.insetBy(dx: 25.0, dy: 0.0)
                var addLine: Bool = false
                
                if indexPath.row == 0 && indexPath.row == ( tableView.numberOfRows(inSection: indexPath.section) - 1) {
                    path.addRoundedRect(in: bounds, cornerWidth: cornerRadius, cornerHeight: cornerRadius)
                    
                } else if indexPath.row == 0 {
                    path.move(to: CGPoint(x: bounds.minX, y: bounds.maxY))
                    path.addArc(tangent1End: CGPoint(x: bounds.minX, y: bounds.minY), tangent2End: CGPoint(x: bounds.midX, y: bounds.minY), radius: cornerRadius)
                    path.addArc(tangent1End: CGPoint(x: bounds.maxX, y: bounds.minY), tangent2End: CGPoint(x: bounds.maxX, y: bounds.midY), radius: cornerRadius)
                    path.addLine(to: CGPoint(x: bounds.maxX, y: bounds.maxY))
                    addLine = true
                    
                } else if indexPath.row == (tableView.numberOfRows(inSection: indexPath.section) - 1) {
                    path.move(to: CGPoint(x: bounds.minX, y: bounds.minY))
                    path.addArc(tangent1End: CGPoint(x: bounds.minX, y: bounds.maxY), tangent2End: CGPoint(x: bounds.midX, y: bounds.maxY), radius: cornerRadius)
                    path.addArc(tangent1End: CGPoint(x: bounds.maxX, y: bounds.maxY), tangent2End: CGPoint(x: bounds.maxX, y: bounds.midY), radius: cornerRadius)
                    path.addLine(to: CGPoint(x: bounds.maxX, y: bounds.minY))
                    
                } else {
                    path.addRect(bounds)
                    addLine = true
                }
                
                layer.path = path
                layer.fillColor = UIColor.white.withAlphaComponent(0.8).cgColor
                
                if addLine {
                    let lineLayer: CALayer = CALayer()
                    let lineHeight: CGFloat = 5 / UIScreen.main.scale
                    lineLayer.frame = CGRect(x: bounds.minX + 10.0, y: bounds.size.height - lineHeight, width: bounds.size.width - 20, height: lineHeight)
                    lineLayer.backgroundColor = UIColor(white: 240/255, alpha: 1).cgColor
                    layer.addSublayer(lineLayer)
                }
                
                let testView: UIView = UIView(frame: bounds)
                testView.layer.insertSublayer(layer, at: 0)
                testView.backgroundColor = .clear
                cell.backgroundView = testView
            }
        }
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath {
        case [0,0],[1,0],[3,1]:
            let cell = tableView.dequeueReusableCell(withIdentifier: "BasicCell", for: indexPath) as! BasicCell
            cell.title.text = line[indexPath.section].value[indexPath.row]
            cell.detail.textColor = indexPath == [0,0] ? color : UIColor.black
            cell.contentView.layer.cornerRadius = 10
            cell.contentView.layer.masksToBounds = true
            return cell
        case [0,1],[2,5]:
            let cell = tableView.dequeueReusableCell(withIdentifier: "SwitchCell", for: indexPath) as! SwitchCell
            cell.title.text = line[indexPath.section].value[indexPath.row]
            return cell
        case [2,3]:
            let cell = tableView.dequeueReusableCell(withIdentifier: "ColorCell", for: indexPath) as! ColorCell
            cell.title.text = line[indexPath.section].value[indexPath.row]
            return cell
        case [1,1],[1,2],[2,1],[3,1]:
            let cell = tableView.dequeueReusableCell(withIdentifier: "DetailCell", for: indexPath) as! DetailCell
            cell.title.text = line[indexPath.section].value[indexPath.row]
            cell.title.textColor = indexPath.section == 1 ? self.color : UIColor.black
            cell.detail.isHidden = true
            return cell
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "DetailCell", for: indexPath) as! DetailCell
            cell.title.text = line[indexPath.section].value[indexPath.row]
            return cell
        }
    }

}
