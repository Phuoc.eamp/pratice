//: Playground - noun: a place where people can play

import UIKit

var str = "helo p"
print(String(str.reversed())) // reversed
print(str.uppercased())
let quote = "The revolution will be Swift"
let substring = quote.dropFirst(23)
let realString = String(substring)
print(realString)



var strA = String(repeating: "a", count: 5)

// use map to change string
var b = str.map {  Character -> Character in
    var a = Character
    a = "m"
    return a
}
// trim()
var c = str.filter {$0 != " "}
print(String(c))

// converse String to Dictionary that key and value is each characters in String
var dict = [String: String]()

var e = str.reduce(dict) { (dic: [String:String], character: Character) -> [String:String] in
            var d = dic
            d[String(character)] = String(character)
            return d
        }
print(e)
//converse String to Dictionary that keys are numbers and values are each characters in String

var dictF = [Int: String]()
str.enumerated().forEach {  (a: Int,b: Character) in
    dictF[a] = String(b)
}

print(dictF)
print(String(c))
print(String(b))
print(Array(str)[1])

// Count the occurrences of characters in String

func repeatOfCharacter1(str: String) -> [String: Int]{
    var dict = [String: Int]()
    str.forEach { (character: Character) in
        dict[String(character)] = Array(str).reduce(0, { (result: Int, characterr: Character) -> Int in
            character == characterr ? result + 1 : result
        })
    }
    return dict
}
func repeatOfCharacter2(str: String) -> [String: Int]{
    var dict = [String: Int]()
    str.map { (character: Character) in
        dict[String(character)] = str.reduce(0, { (result: Int, characterr: Character) -> Int in
            character == characterr ? result + 1 : result
        })
    }
    return dict
}
func repeatOfCharacter3(str: String) -> [String: Int]{
    var dict = [String: Int]()
    str.reduce(dict) { (dic: [String: Int], character: Character) -> [String: Int] in
        
        dict[String(character)] = str.reduce(0, { (result: Int, characterr: Character) -> Int in
            character == characterr ? result + 1 : result
        })
        return dict
    }
    return dict
}

print(repeatOfCharacter1(str: "abca"))
print(repeatOfCharacter2(str: "aba"))
print(repeatOfCharacter3(str: "abca"))

// print and uppercase in String charracter

let index = str.index(str.startIndex, offsetBy: 4)
var a = String(str[index]).uppercased()
print(a)

// Extensive String

var name = "nGuyEn Van anh"

extension String{
    
    // update character in String
    func updateCharacter(at position: Int,by character: Character) -> String{
        var string = self
        string.remove(at: string.index(string.startIndex, offsetBy: position - 1))
        string.insert(character, at: string.index(string.startIndex, offsetBy: position - 1))
        return string
    }
    
    // uppercased character in String
    func uppcasedCharacter(at position: Int) -> String{
        var string = self
        let a = String(string.remove(at: string.index(string.startIndex, offsetBy: position - 1)))
        string.insert(Character(a.uppercased()), at: string.index(string.startIndex, offsetBy: position - 1))
        return string
    }
    // lowercased character in String
    func lowercasedCharacter(at position: Int) -> String{
        var string = self
        let a = String(string.remove(at: string.index(string.startIndex, offsetBy: position - 1)))
        string.insert(Character(a.lowercased()), at: string.index(string.startIndex, offsetBy: position - 1))
        return string
    }
    
    // converse to Vietnamese name
    func converseName() -> String{
        var string = self.lowercased()
//
//
//      self.enumerated().forEach { (offset: Int, element: Character) in
//            string = element == " " ? string.uppcasedCharacter(at: offset + 2) : string
//      }
//
        for (offset,element) in self.enumerated() where element == " "{
            string = string.uppcasedCharacter(at: offset + 2)
        }
        return string.uppcasedCharacter(at: 1)
    }
}

print("My name is \(name.converseName())")

//sub String
let greeting = "Hello, world!"
let subIndex = greeting.index(of: ",") ?? greeting.endIndex
var first = greeting[..<subIndex]

print(greeting.filter{$0 != ","})

//
let fullname = ["Nguyen Van A","tRan van B","vo Van C","Nguyen van D","Nguyen van E", "Tran thi f"]


func lastName(name: [String], lastname: String){
    print("There are \(fullname.filter {$0.lowercased().hasPrefix(lastname.lowercased())}.count) people have last name is \(lastname.converseName())")
}
lastName(name: fullname, lastname: "trAn")

//print("There are \(fullname.filter {$0.lowercased().hasPrefix("nguyen")}.count) people have last name is Nguyen")

let names = fullname.map {$0.converseName()}

print(names)
var dictLastName = [String : Int]()
let lastnames = names.map { (string: String) -> String in
    let index = string.index(of: " ") ?? string.endIndex
    return String(string[..<index])
}

print(lastnames)
lastnames.map { (string: String) in
    dictLastName[string] = lastnames.reduce(0, { (result: Int, stringg: String) in
        string == stringg ? result + 1 : result
    })
}
print(dictLastName)

extension String{
    func updateString(str: String){
        
    }
}
str.hashValue

