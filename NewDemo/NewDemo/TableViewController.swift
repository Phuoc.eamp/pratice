//
//  TableViewController.swift
//  NewDemo
//
//  Created by PhuocNguyen on 12/16/18.
//  Copyright © 2018 PhuocNguyen. All rights reserved.
//

import UIKit

class TableViewController: UITableViewController {
    var line = ["Account": ["Signed In","Drive Detection"],
                "Subscription": ["Free","Get Unlimited Drives","Support Simple"],
                "Personalization": ["Vechiles","Custom Purposes","Mileage Rates","Theme","Language","Use Imperial Units(Miles)"],
                "Other Setting": ["Rate Made Simple","Version"]]
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return line.count
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return line.keys.sorted()[section]
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return line.keys.sorted()[section].count
    }
    
    
}
