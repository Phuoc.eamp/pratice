//
//  ViewController.swift
//  Touches
//
//  Created by PhuocNguyen on 11/23/18.
//  Copyright © 2018 PhuocNguyen. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    var scale: CGFloat = 1

    @IBOutlet var tapGR: UITapGestureRecognizer!
    @IBOutlet var panGR: UIPanGestureRecognizer!
    @IBOutlet var rotationGR: UIRotationGestureRecognizer!
    @IBOutlet var pinchGR: UIPinchGestureRecognizer!
    override func viewDidLoad() {
        super.viewDidLoad()
        panGR.minimumNumberOfTouches = 2
        pinchGR.scale = 2
        rotationGR.rotation = 1
    }
    @IBOutlet var swipeGR: UISwipeGestureRecognizer!
    
    @IBAction func tapImage(_ sender: UITapGestureRecognizer) {
        guard scale == 1 else {
            sender.view?.transform = (sender.view?.transform.scaledBy(x: 1/2, y: 1/2))!
            scale = 1
            return
        }
        sender.view?.transform = (sender.view?.transform.scaledBy(x: 2, y: 2))!
        scale = 2
    }
    
    @IBAction func swipeImage(_ sender: UISwipeGestureRecognizer) {
        sender.view?.alpha = sender.view?.alpha == 1 ? 0.5 : 1
    }
    
    @IBAction func pinchImage(_ sender: UIPinchGestureRecognizer) {
        sender.view?.transform = (sender.view?.transform.scaledBy(x: sender.scale, y: sender.scale))!
        pinchGR.scale = 1
    }
    
    @IBAction func rotationImage(_ sender: UIRotationGestureRecognizer) {
        sender.view?.transform = (sender.view?.transform.rotated(by: sender.rotation))!
        sender.rotation = 0
    }
    @IBAction func panImage(_ sender: UIPanGestureRecognizer) {
        let position = sender.translation(in: view)
        sender.view?.transform = (sender.view?.transform.translatedBy(x: position.x, y: position.y))!
        sender.setTranslation(CGPoint.zero, in: view)
    }
    
}

