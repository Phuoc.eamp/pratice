//: Playground - noun: a place where people can play

import UIKit
import CoreGraphics
// use subcript in array to print array[Int,Int] -> odd or even
extension Array {
    subscript(index: (Int,Int)) -> (String,String)?{
        guard let value = self[index.0] as? Int else {
            return nil
        }
        switch (value >= 0, abs(value) % 2) {
        case (true, 0):
            return ("positive", "even")
        case (true, 1):
            return ("positive", "odd")
        case (false, 0):
            return ("negative", "even")
        case (false, 1):
            return ("negative", "odd")
        default:
            return nil
        }
    }
}
extension String {
    subscript(index: Int) -> Character {
        assert(index < self.count , "limited")
        return self[self.index(startIndex, offsetBy: index)]
    }
}
var str = "helo"

str[3]
var arr = [1,2,3]
arr[(1,1)]
print(arr[1])

//use subcript to define operate ^
protocol Exponent {
    static func ^(base: Self, exponent: Int) -> Float
}

extension Float: Exponent {
    static func ^(base: Float, exponent: Int) -> Float{
        var result: Float = 1.0
        assert(exponent > 0, "No Calculated")
        for _ in 1...exponent {
            result *= base
        }
        return result
    }
    
}
struct point: Equatable {
    var a: Float
    
}

let exponent:Int = 2
let baseDouble: Float = 2.9
let a: Float = 2.0
baseDouble ^ exponent

//var resultDouble = baseDouble *** exponent
