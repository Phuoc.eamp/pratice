//
//  ViewController.swift
//  CustomTableViewCell
//
//  Created by PhuocNguyen on 12/4/18.
//  Copyright © 2018 PhuocNguyen. All rights reserved.
//

import UIKit

class MyTableViewController: UITableViewController {
    var item = ["Item1", "Item2", "Item3"]
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "My Table View"
        tableView.register(MyCell.self, forCellReuseIdentifier: "Cell")
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return item.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! MyCell
        cell.label.text = item[indexPath.row]
        cell.myTableViewController = self
        return cell
    }
    
    func deleteCell(cell: UITableViewCell) {
        if let deleteIndexPath = tableView.indexPath(for: cell) {
            item.remove(at: deleteIndexPath.row)
            tableView.deleteRows(at: [deleteIndexPath], with: .automatic)
        }
    }

}

class MyCell: UITableViewCell {
    var myTableViewController: MyTableViewController?
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    let label: UILabel = {
        let label = UILabel()
        label.text = "Item"
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let actionBtn: UIButton = {
        let actionBtn = UIButton(type: .system)
        actionBtn.setTitle("Delete", for: .normal)
        actionBtn.translatesAutoresizingMaskIntoConstraints = false
        return actionBtn
    }()
    
    func setupView() {
        addSubview(label)
        addSubview(actionBtn)
        actionBtn.addTarget(self, action: #selector(handleTap(_:)), for: .touchUpInside)
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-16-[v0]-8-[v1(80)]-8-|", options: NSLayoutConstraint.FormatOptions(), metrics: nil, views: ["v0" : label, "v1": actionBtn]))
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-10-[v0]", options: NSLayoutConstraint.FormatOptions(), metrics: nil, views: ["v0" : label]))
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-10-[v0]", options: NSLayoutConstraint.FormatOptions(), metrics: nil, views: ["v0" : actionBtn]))
    }
    
    @objc func handleTap(_ sender: UIButton!) {
        myTableViewController?.deleteCell(cell: self)
    }
}

