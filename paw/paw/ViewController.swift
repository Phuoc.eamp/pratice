//
//  ViewController.swift
//  paw
//
//  Created by PhuocNguyen on 11/15/18.
//  Copyright © 2018 PhuocNguyen. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    var colorBtn = UIColor(white: 230/255, alpha: 1)
    //IBoulet topview
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var nameLB: UILabel!
    @IBOutlet weak var titleLB: UILabel!
    @IBOutlet weak var avarImage: UIImageView!
    @IBOutlet weak var addressLB: UILabel!
    @IBOutlet weak var emailLB: UILabel!
    //IBoulet midview
    @IBOutlet weak var midView: UIView!
    @IBOutlet weak var infoView: UIView!
    @IBOutlet weak var languageLB: UILabel!
    @IBOutlet weak var currencyLB: UILabel!
    @IBOutlet weak var unitsLB: UILabel!
    @IBOutlet weak var remindersLB: UILabel!
    @IBOutlet weak var privacyLB: UILabel!
    @IBOutlet weak var languageValueLB: UILabel!
    @IBOutlet weak var currencyValueLB: UILabel!
    @IBOutlet weak var unitsValueLB: UILabel!
    @IBOutlet weak var loginView: UIView!
    @IBOutlet weak var reminderSwitch: UISwitch!
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var spaceView1: UIView!
    @IBOutlet weak var spaceView2: UIView!
    @IBOutlet weak var spaceView3: UIView!
    @IBOutlet weak var spaceView4: UIView!
    @IBOutlet weak var spaceView5: UIView!
    //IBoulet bottView
    @IBOutlet weak var bottView: UIView!
    @IBOutlet weak var btnTravel: UIButton!
    @IBOutlet weak var btnFlight: UIButton!
    @IBOutlet weak var btnChat: UIButton!
    @IBOutlet weak var btnHotel: UIButton!
    @IBOutlet weak var btnProfile: UIButton!
    @IBOutlet weak var notiLB: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //setupView()
        //setupImage()
        setupUI()
    }
    
    //use to setup some SubView
    func setupUI() {
//        nameLB.fomartMainTilte(size: 20)
//        titleLB.fomartMainTilte(size: topView.frame.height/5)
        titleLB.font = UIFont.boldSystemFont(ofSize: topView.frame.height/8)
        btnLogin.layer.cornerRadius = 10
        avarImage.layer.cornerRadius = avarImage.frame.height/2
        avarImage.clipsToBounds = true
        
    }
    
    @available(iOS 11.0, *)
    func setupView() {
        setupTopView()
        setupMidView()
        setupBottView()
    }

    // Setup Top view
    @available(iOS 11.0, *)
    func setupTopView() {
        topView.translatesAutoresizingMaskIntoConstraints = false
        topView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
        topView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        topView.widthAnchor.constraint(equalTo: view.safeAreaLayoutGuide.widthAnchor).isActive = true
        topView.heightAnchor.constraint(equalTo: view.safeAreaLayoutGuide.heightAnchor, multiplier: 0.25).isActive = true
        //topView.backgroundColor = UIColor.orange
        // title label
        titleLB.text = "Profile"
        titleLB.font = UIFont.boldSystemFont(ofSize: topView.frame.height/5)
        //titleLB.heightAnchor.constraint(equalTo: topView.heightAnchor, multiplier: 0.3)
        titleLB.translatesAutoresizingMaskIntoConstraints = false
        titleLB.topAnchor.constraint(equalTo: topView.topAnchor).isActive = true
        titleLB.leadingAnchor.constraint(equalTo: topView.leadingAnchor).isActive = true
        //avar image
        avarImage.translatesAutoresizingMaskIntoConstraints = false
        avarImage.topAnchor.constraint(equalTo: titleLB.bottomAnchor).isActive = true
        //avarImage.bottomAnchor.constraint(equalTo: topView.bottomAnchor).isActive = true
        avarImage.heightAnchor.constraint(equalTo: topView.heightAnchor, multiplier: 0.6).isActive = true
        //avarImage.widthAnchor.constraint(equalTo: avarImage.heightAnchor).isActive = true
        avarImage.widthAnchor.constraint(equalTo: avarImage.heightAnchor).isActive = true
        avarImage.leadingAnchor.constraint(equalTo: topView.leadingAnchor).isActive = true
        //avarImage.backgroundColor = UIColor.yellow
        avarImage.image = UIImage(named: "hinh2.jpg")
        avarImage.layer.cornerRadius = avarImage.frame.height/1.5
        avarImage.clipsToBounds = true
//        avarImage.layer.borderWidth = 3
//        avarImage.layer.borderColor = UIColor.white.cgColor
        // name
        nameLB.translatesAutoresizingMaskIntoConstraints = false
        nameLB.leadingAnchor.constraint(equalTo: avarImage.trailingAnchor, constant: 10).isActive = true
        nameLB.topAnchor.constraint(equalTo: avarImage.topAnchor).isActive = true
        //nameLB.font = UIFont.boldSystemFont(ofSize: 20)
        nameLB.text = "Paw Teamate"
        
        nameLB.textColor = UIColor.orange
    }

    // Setup Mid View
    @available(iOS 11.0, *)
    func setupMidView() {
        midView.translatesAutoresizingMaskIntoConstraints = false
        midView.topAnchor.constraint(equalTo: topView.bottomAnchor).isActive = true
        midView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        midView.heightAnchor.constraint(equalTo: view.heightAnchor, multiplier: 0.65).isActive = true
        midView.widthAnchor.constraint(equalTo: view.safeAreaLayoutGuide.widthAnchor).isActive = true

        infoView.translatesAutoresizingMaskIntoConstraints = false
        infoView.centerXAnchor.constraint(equalTo: midView.centerXAnchor).isActive = true
        infoView.topAnchor.constraint(equalTo: midView.topAnchor).isActive = true
        infoView.widthAnchor.constraint(equalTo: midView.widthAnchor).isActive = true
        infoView.backgroundColor = .gray
        infoView.heightAnchor.constraint(equalTo: midView.heightAnchor, multiplier: 0.7).isActive = true
        setupInfoView()

        loginView.translatesAutoresizingMaskIntoConstraints = false
        loginView.bottomAnchor.constraint(equalTo: midView.bottomAnchor).isActive = true
        loginView.centerXAnchor.constraint(equalTo: midView.centerXAnchor).isActive = true
        loginView.widthAnchor.constraint(equalTo: midView.widthAnchor).isActive = true
        loginView.heightAnchor.constraint(equalTo: midView.heightAnchor, multiplier: 0.3).isActive = true
        setupLoginView()
    }

    func setupInfoView() {
        setupSpaceView(spaceView: spaceView1)
        spaceView1.topAnchor.constraint(equalTo: infoView.topAnchor).isActive = true
        languageLB.centerYAnchor.constraint(equalTo: spaceView1.centerYAnchor).isActive = true
        setupLeftLabel(label: languageLB, title: "Language")
        languageValueLB.centerYAnchor.constraint(equalTo: spaceView1.centerYAnchor).isActive = true
        setupRightLabel(label: languageValueLB, title: "English")

        setupSpaceView(spaceView: spaceView2)
        spaceView2.topAnchor.constraint(equalTo: spaceView1.bottomAnchor).isActive = true
        currencyLB.centerYAnchor.constraint(equalTo: spaceView2.centerYAnchor).isActive = true
        setupLeftLabel(label: currencyLB, title: "Currency")
        currencyValueLB.centerYAnchor.constraint(equalTo: spaceView2.centerYAnchor).isActive = true
        setupRightLabel(label: currencyValueLB, title: "USD")

        setupSpaceView(spaceView: spaceView3)
        spaceView3.topAnchor.constraint(equalTo: spaceView2.bottomAnchor).isActive = true
        remindersLB.centerYAnchor.constraint(equalTo: spaceView3.centerYAnchor).isActive = true
        setupLeftLabel(label: remindersLB, title: "Reminders")
        reminderSwitch.translatesAutoresizingMaskIntoConstraints = false
        reminderSwitch.centerYAnchor.constraint(equalTo: spaceView3.centerYAnchor).isActive = true
        //reminderSwitch.widthAnchor.constraint(equalToConstant: 10).isActive = true
        reminderSwitch.trailingAnchor.constraint(equalTo: midView.trailingAnchor).isActive = true
        reminderSwitch.heightAnchor.constraint(equalTo: spaceView3.heightAnchor, multiplier: 0.3).isActive = true

        setupSpaceView(spaceView: spaceView4)
        spaceView4.topAnchor.constraint(equalTo: spaceView3.bottomAnchor).isActive = true
        unitsLB.centerYAnchor.constraint(equalTo: spaceView4.centerYAnchor).isActive = true
        setupLeftLabel(label: unitsLB, title: "Units")
        unitsValueLB.centerYAnchor.constraint(equalTo: spaceView4.centerYAnchor).isActive = true
        setupRightLabel(label: unitsValueLB, title: "Imperial")

        setupSpaceView(spaceView: spaceView5)
        spaceView5.topAnchor.constraint(equalTo: spaceView4.bottomAnchor).isActive = true
        privacyLB.centerYAnchor.constraint(equalTo: spaceView5.centerYAnchor).isActive = true
        setupLeftLabel(label: privacyLB, title: "Privacy Policy")
    }

    func setupLoginView() {
        btnLogin.translatesAutoresizingMaskIntoConstraints = false
        btnLogin.topAnchor.constraint(equalTo: loginView.topAnchor, constant: 5).isActive = true
        btnLogin.heightAnchor.constraint(equalTo: loginView.heightAnchor, multiplier: 0.4).isActive = true
        btnLogin.centerXAnchor.constraint(equalTo: loginView.centerXAnchor).isActive = true
        //btnLogin.widthAnchor.constraint(equalTo: midView.widthAnchor).isActive = true
        btnLogin.widthAnchor.constraint(lessThanOrEqualToConstant: 300).isActive = true
        let widthConstant = btnLogin.widthAnchor.constraint(equalTo: loginView.widthAnchor, multiplier: 0.7)
        widthConstant.priority = UILayoutPriority(rawValue: 500)
        widthConstant.isActive = true
        btnLogin.backgroundColor = colorBtn
        btnLogin.setTitle("Sign Out", for: .normal)
        btnLogin.setTitleColor(UIColor.orange, for: .normal)
        btnLogin.layer.cornerRadius = 20
        btnLogin.clipsToBounds = true
    }

    func setupLeftLabel(label: UILabel, title: String) {
        label.text = title
        label.translatesAutoresizingMaskIntoConstraints = false
        label.leadingAnchor.constraint(equalTo: midView.leadingAnchor).isActive = true
    }

    func setupRightLabel(label: UILabel, title: String) {
        label.text = title
        label.textColor = UIColor.gray
        label.translatesAutoresizingMaskIntoConstraints = false
        label.trailingAnchor.constraint(equalTo: midView.trailingAnchor).isActive = true
    }

    func setupSpaceView(spaceView: UIView) {
        spaceView.translatesAutoresizingMaskIntoConstraints = false
        spaceView.heightAnchor.constraint(equalTo: infoView.heightAnchor, multiplier: 0.2).isActive = true
        spaceView.widthAnchor.constraint(equalTo: infoView.widthAnchor).isActive = true
        spaceView.centerXAnchor.constraint(equalTo: infoView.centerXAnchor).isActive = true
//        spaceView.layer.borderWidth = 1
//        spaceView.layer.borderColor = colorBtn.cgColor
    }
    // setup Bottom View
    func setupBottView() {
        bottView.translatesAutoresizingMaskIntoConstraints = false
        bottView.topAnchor.constraint(equalTo: midView.bottomAnchor).isActive = true
        bottView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        bottView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        bottView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        bottView.backgroundColor = colorBtn
        // design button
        setupBotBtn(btn: btnTravel, title: "Travel")
        btnTravel.leadingAnchor.constraint(equalTo: bottView.leadingAnchor).isActive = true
        setupBotBtn(btn: btnFlight, title: "Flights")
        btnFlight.leadingAnchor.constraint(equalTo: btnTravel.trailingAnchor).isActive = true
        setupBotBtn(btn: btnChat, title: "Chatbot")
        btnChat.leadingAnchor.constraint(equalTo: btnFlight.trailingAnchor).isActive = true
        setupBotBtn(btn: btnHotel, title: "Hotel")
        btnHotel.leadingAnchor.constraint(equalTo: btnChat.trailingAnchor).isActive = true
        setupBotBtn(btn: btnProfile, title: "Profile")
        btnProfile.leadingAnchor.constraint(equalTo: btnHotel.trailingAnchor).isActive = true

    }

    func setupBotBtn(btn: UIButton, title: String) {
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.topAnchor.constraint(equalTo: bottView.topAnchor).isActive = true
        btn.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        btn.widthAnchor.constraint(equalTo: bottView.widthAnchor, multiplier: 0.2).isActive = true
        btn.heightAnchor.constraint(equalTo: bottView.heightAnchor).isActive = true
        btn.setTitle(title, for: .normal)
        btn.setTitleColor(UIColor.gray, for: .normal)
        //btn.layer.backgroundColor = colorBtn.cgColor
    }
}

