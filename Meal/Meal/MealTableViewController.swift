//
//  MealTableViewController.swift
//  Meal
//
//  Created by PhuocNguyen on 12/11/18.
//  Copyright © 2018 PhuocNguyen. All rights reserved.
//

import UIKit
import os.log

class MealTableViewController: UITableViewController {
    var meal = [Meal]()
    override func viewDidLoad() {
        super.viewDidLoad()
        if let savedMeals = loadMeals(){
            self.meal += savedMeals
        }
//        tableView.register(MealHeader.self, forHeaderFooterViewReuseIdentifier: "HeadID")
//        tableView.sectionHeaderHeight = 100
        
    }

//    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//        return tableView.dequeueReusableHeaderFooterView(withIdentifier: "HeadID")
//    }
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.meal.count
    }
    
    override func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let deleteAction = UITableViewRowAction(style: .default, title: "Delete") { (adtion, indexPath) in
            self.deleteAction(indexPath: indexPath)
        }
        deleteAction.backgroundColor = .red
        return [deleteAction]
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! MealTableViewCell
        cell.mealNameLabel.text = meal[indexPath.row].name
        cell.mealImageView.image = meal[indexPath.row].photo
        cell.mealImageView.layer.cornerRadius = cell.mealImageView.frame.height/2
        cell.mealImageView.clipsToBounds = true
        cell.starStackView.star = meal[indexPath.row].star
        //cell.timeLabel.text = meal[indexPath.row].timeCreated?.description
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy HH:mm"
        if let timeCreated = meal[indexPath.row].timeCreated {
            if let timeCreated = meal[indexPath.row].timeUpdated {
                cell.timeLabel.text = dateFormatter.string(from: timeCreated)
            }else{
                cell.timeLabel.text = dateFormatter.string(from: timeCreated)
            }
        }
//        cell.timeLabel.text = meal[indexPath.row].timeUpdated != nil ? dateFormatter.string(from: meal[indexPath.row].timeUpdated!) : dateFormatter.string(from: meal[indexPath.row].timeCreated!)
        cell.descriptionMealLabel.text = meal[indexPath.row].descriptionMeal
        cell.accessoryType = .checkmark
        cell.tintColor = UIColor.red
        return cell
    }
    
    func deleteAction(indexPath: IndexPath){
        let alert = UIAlertController(title: "Delete", message: "Do you want to delete this meal ?", preferredStyle: .alert)
        let deleteAction = UIAlertAction(title: "Yes", style: .default) { (action) in
            self.meal.remove(at: indexPath.row)
            self.saveMeals()
            self.tableView.deleteRows(at: [indexPath], with: .bottom)
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)
        alert.addAction(deleteAction)
        alert.addAction(cancelAction)
        present(alert, animated: true, completion: nil)
    }
    
    @IBAction func saveMeal(segue: UIStoryboardSegue){
        print("Save")
        if let addMeal = segue.source as? MealDetailViewController, let meal = addMeal.meal{
            print("ok")
            if let selectIndexPath = tableView.indexPathForSelectedRow{
                self.meal[selectIndexPath.row] = meal
                self.meal.sort()
                tableView.reloadData()
            }else{
                self.meal.append(meal)
                self.meal.sort()
                tableView.reloadData()
            }
        }
        self.saveMeals()
    }
    
    @IBAction func cancelMeal(segue: UIStoryboardSegue){
        dismiss(animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        if segue.identifier == "ShowDetail"{
            guard let mealDetailViewController = segue.destination as? MealDetailViewController else{return}
            guard let selectedMealCell = sender as? MealTableViewCell else{return}
            guard let indexPath = tableView.indexPath(for: selectedMealCell) else{return}
            let selectedMeal = meal[indexPath.row]
            mealDetailViewController.meal = selectedMeal
            mealDetailViewController.timeCreated = selectedMeal.timeCreated
            print(indexPath)
            //print("aaaa")
        }
    }
    
    @IBAction func sortByTime(_ sender: Any) {
        self.meal.sort { (lhsMeal, rhsMeal) -> Bool in
            guard let lhsTimeCreated = lhsMeal.timeCreated, let rhsTimeCreated = rhsMeal.timeCreated else {return false}
            let lhsTime = lhsMeal.timeUpdated ?? lhsTimeCreated
            let rhsTime = rhsMeal.timeUpdated ?? rhsTimeCreated
            return lhsTime < rhsTime
        }
        print(meal.map({$0.timeCreated}))
        print(meal.map({$0.timeUpdated}))
        tableView.reloadData()
        print("Sort")
    }
    private func saveMeals() {
        let isSuccessfulSave = NSKeyedArchiver.archiveRootObject(meal, toFile: Meal.ArchiveURL.path)
        if isSuccessfulSave {
            os_log("Meals successfully saved.", log: OSLog.default, type: .debug)
        } else {
            os_log("Failed to save meals...", log: OSLog.default, type: .error)
        }
    }
    
    private func loadMeals() -> [Meal]?  {
        return NSKeyedUnarchiver.unarchiveObject(withFile: Meal.ArchiveURL.path) as? [Meal]
    }

}

//class MealHeader: UITableViewHeaderFooterView{
//    override init(reuseIdentifier: String?) {
//        super.init(reuseIdentifier: reuseIdentifier)
//        setupViews()
//    }
//
//    required init?(coder aDecoder: NSCoder) {
//        fatalError("init(coder:) has not been implemented")
//    }
//
//    let titleButton = ["SORT","Name","Date","Rating"]
//    let stackView: UIStackView = {
//        let stackView = UIStackView()
//        stackView.axis = .vertical
//        stackView.translatesAutoresizingMaskIntoConstraints = false
//        stackView.widthAnchor.constraint(equalToConstant: 150)
//        return stackView
//    }()
//    func setupViews(){
//        self.backgroundColor = UIColor.black
//        let label = UILabel()
//        label.text = "Header"
//        label.font = UIFont.boldSystemFont(ofSize: 20)
//        label.translatesAutoresizingMaskIntoConstraints = false
////        let button = UIButton()
////        button.translatesAutoresizingMaskIntoConstraints = false
////        button.widthAnchor.constraint(equalToConstant: 150)
////        button.heightAnchor.constraint(equalToConstant: 25)
////        button.titleLabel?.text = "Sort"
////
//        //setupStackView()
//        addSubview(label)
//        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-16-[v0]|", options: NSLayoutConstraint.FormatOptions(), metrics: nil, views: ["v0": label]))
//        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[v0]", options: NSLayoutConstraint.FormatOptions(), metrics: nil, views: ["v0": label]))
//    }
//
//    func setupStackView(){
//            let button = UIButton()
//            button.translatesAutoresizingMaskIntoConstraints = false
//            button.widthAnchor.constraint(equalToConstant: 150)
//            button.heightAnchor.constraint(equalToConstant: 25)
//            button.titleLabel?.text = "Sort"
//            stackView.addArrangedSubview(button)
//    }
//}
//
