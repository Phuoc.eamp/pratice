//
//  StarView.swift
//  Meal
//
//  Created by PhuocNguyen on 12/11/18.
//  Copyright © 2018 PhuocNguyen. All rights reserved.
//

import UIKit
@IBDesignable class StarView: UIStackView {
    
    var ratingStars = [UIButton]()
    var star = 0 {
        didSet{
            updateStar()
        }
    }
    @IBInspectable var starSize: CGSize = CGSize(width: 44, height: 44) {
        didSet{
            setupButton()
        }
    }
    var starCount = 5{
        didSet{
            setupButton()
        }
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupButton()
    }
    
    required init(coder: NSCoder) {
        super.init(coder: coder)
        setupButton()
    }
    
    //    required init(coder: NSCoder) {
    //        fatalError("init(coder:) has not been implemented")
    //
    //    }
    
    func setupButton() {
        for star in ratingStars{
            removeArrangedSubview(star)
            star.removeFromSuperview()
        }
        ratingStars.removeAll()
        let bundle = Bundle(for: type(of: self))
        let filledStar = UIImage(named: "FilledStar", in: bundle, compatibleWith: self.traitCollection)
        let emptyStar = UIImage(named:"EmptyStar", in: bundle, compatibleWith: self.traitCollection)
        let highlightedStar = UIImage(named:"HighlightedStar", in: bundle, compatibleWith: self.traitCollection)
        for _ in 0..<starCount{
            let star = UIButton()
            star.setImage(emptyStar, for: .normal)
            star.setImage(filledStar, for: .selected)
            star.setImage(highlightedStar, for: .highlighted)
            star.setImage(highlightedStar, for: [.highlighted, .selected])
            star.translatesAutoresizingMaskIntoConstraints = false
            star.heightAnchor.constraint(equalToConstant: starSize.height).isActive = true
            star.widthAnchor.constraint(equalToConstant: starSize.width).isActive = true
            //star.addTarget(self, action: #selector(tapStar(button:)), for: .touchUpInside)
            ratingStars.append(star)
            addArrangedSubview(star)
        }
        
    }
    
//    @objc func tapStar(button: UIButton){
//        guard let index = ratingStars.lastIndex(of: button) else { return }
//        let selectedStar = index + 1
//        star = star == selectedStar ? 0 : selectedStar
//        print(self.star)
//    }
    
    func updateStar(){
        ratingStars.enumerated().forEach { (offset: Int, element: UIButton) in
            element.isSelected = offset < star
        }
    }
    
}
