//
//  MealTableViewCell.swift
//  Meal
//
//  Created by PhuocNguyen on 12/11/18.
//  Copyright © 2018 PhuocNguyen. All rights reserved.
//

import UIKit

class MealTableViewCell: UITableViewCell {
    @IBOutlet weak var mealImageView: UIImageView!
    @IBOutlet weak var mealNameLabel: UILabel!
    @IBOutlet weak var starStackView: StarView!
    
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var descriptionMealLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        descriptionMealLabel.numberOfLines = 3
        descriptionMealLabel.sizeToFit()
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
