//
//  Meal.swift
//  Meal
//
//  Created by PhuocNguyen on 12/11/18.
//  Copyright © 2018 PhuocNguyen. All rights reserved.
//

import Foundation
import UIKit
import os.log

struct PropertyKey {
    static let name = "name"
    static let photo = "photo"
    static let star = "star"
    static let timeUpdated = "timeupdated"
    static let timeCreated = "timecreated"
    static let descriptionMeal = "descriptionMeal"
}

class Meal: NSObject, Comparable, NSCoding {
    var name: String
    var photo: UIImage?
    var star: Int
    var timeCreated: Date?
    var timeUpdated: Date?
    var descriptionMeal: String?

    func encode(with aCoder: NSCoder) {
        aCoder.encode(name, forKey: PropertyKey.name)
        aCoder.encode(photo, forKey: PropertyKey.photo)
        aCoder.encode(star, forKey: PropertyKey.star)
        aCoder.encode(timeCreated, forKey: PropertyKey.timeCreated)
        aCoder.encode(timeUpdated, forKey: PropertyKey.timeUpdated)
        aCoder.encode(descriptionMeal, forKey: PropertyKey.descriptionMeal)
    }
    
    init?(name: String,photo: UIImage?,star: Int, timeCreated: Date?, timeUpdated: Date?, descriptionMeal: String?) {
        guard !name.isEmpty else {return nil}
        guard star >= 0 && star <= 5 else{return nil}
        self.name = name
        self.photo = photo
        self.star = star
        self.timeCreated = timeCreated
        self.timeUpdated = timeUpdated
        self.descriptionMeal = descriptionMeal
    }
    
    required convenience init?(coder aDecoder: NSCoder) {
        guard let name = aDecoder.decodeObject(forKey: PropertyKey.name) as? String else {
            os_log("Unable to decode the name for a Meal object.", log: OSLog.default, type: .debug)
            return nil
        }
        let photo = aDecoder.decodeObject(forKey: PropertyKey.photo) as? UIImage
        let star = aDecoder.decodeInteger(forKey: PropertyKey.star)
        let timeCreated = aDecoder.decodeObject(forKey: PropertyKey.timeCreated) as? Date
        let timeUpdated = aDecoder.decodeObject(forKey: PropertyKey.timeUpdated) as? Date
        let descriptionMeal = aDecoder.decodeObject(forKey: PropertyKey.descriptionMeal) as? String
        self.init(name: name, photo: photo, star: star, timeCreated: timeCreated, timeUpdated: timeUpdated, descriptionMeal: descriptionMeal)
    }
    
    static let DocumentsDirectory = FileManager().urls(for: .documentDirectory, in: .userDomainMask).first!
    static let ArchiveURL = DocumentsDirectory.appendingPathComponent("meals")
    
    private func loadMeals() -> [Meal]?  {
        return NSKeyedUnarchiver.unarchiveObject(withFile: Meal.ArchiveURL.path) as? [Meal]
    }
    
    static func < (lhs: Meal, rhs: Meal) -> Bool {
        return lhs.name.uppercased() < rhs.name.uppercased()
    }
    
    static func == (lhs: Meal, rhs: Meal) -> Bool {
        return lhs.name.uppercased() == rhs.name.uppercased()
    }
    
    
    
}


