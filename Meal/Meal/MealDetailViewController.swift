//
//  ViewController.swift
//  Meal
//
//  Created by PhuocNguyen on 12/10/18.
//  Copyright © 2018 PhuocNguyen. All rights reserved.
//

import UIKit

class MealDetailViewController: UIViewController, UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    // MARK Outlet
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var starStackView: StarView!
    @IBOutlet weak var mealNameLable: UILabel!
    @IBOutlet weak var mealNameTextField: UITextField!
    @IBOutlet weak var mealImage: UIImageView!
    @IBOutlet weak var saveButtonItem: UIBarButtonItem!
    var meal: Meal?
    var timeCreated: Date?
    override func viewDidLoad() {
        super.viewDidLoad()
        mealNameTextField.delegate = self
        if let meal = self.meal{
            self.mealNameTextField.text = meal.name
            navigationItem.title = meal.name
            self.starStackView.star = meal.star
            self.mealImage.image = meal.photo
            self.descriptionTextView.text = meal.descriptionMeal
        }
        updateSaveButton()
    }
    
    // MARK TextFielddelegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }

    func textFieldDidEndEditing(_ textField: UITextField) {
        mealNameLable.text = textField.text
        navigationItem.title = textField.text
        updateSaveButton()
    }
    
    func updateSaveButton(){
        let text = self.mealNameTextField.text ?? ""
        saveButtonItem.isEnabled = !text.isEmpty
    }
    
    // MARK PickerVIewDelegate
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        let selectImage = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
        mealImage.image = selectImage
        dismiss(animated: true, completion: nil)
    }
    // MARK Action
    @IBAction func selectImage(_ sender: UITapGestureRecognizer) {
        mealNameTextField.resignFirstResponder()
        let imagePickerView = UIImagePickerController()
        imagePickerView.sourceType = .photoLibrary
        imagePickerView.delegate = self
        present(imagePickerView, animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        if segue.identifier == "AddMeal"{
            guard let mealName = self.mealNameTextField.text else{return}
            guard let timeCreated = self.timeCreated else{
                self.meal = Meal(name: mealName, photo: self.mealImage.image, star: self.starStackView.star, timeCreated: Date(), timeUpdated: nil, descriptionMeal: descriptionTextView.text)
                return
            }
            self.meal = Meal(name: mealName, photo: self.mealImage.image, star: self.starStackView.star, timeCreated: timeCreated, timeUpdated: Date(), descriptionMeal: descriptionTextView.text)
        }
    }
}

