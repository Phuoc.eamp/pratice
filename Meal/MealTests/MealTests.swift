//
//  MealTests.swift
//  MealTests
//
//  Created by PhuocNguyen on 12/10/18.
//  Copyright © 2018 PhuocNguyen. All rights reserved.
//

import XCTest
@testable import Meal

class MealTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    func testMealInitializationSucceeds() {
        let zeroStarMeal = Meal.init(name: "Zero", photo: nil, star: 0)
        XCTAssert((zeroStarMeal != nil))
        let higtStarMeal = Meal.init(name: "Positive", photo: nil, star: 5)
        XCTAssert((higtStarMeal != nil))
    }
    
    func testMealInitializationFails() {
        let negativeStarMeal = Meal.init(name: "Negative", photo: nil, star: -1)
        XCTAssertNil(negativeStarMeal)
        let emptyStringMeal = Meal.init(name: "", photo: nil, star: 0)
        XCTAssertNil(emptyStringMeal)
        let largeStarMeal = Meal.init(name: "Large", photo: nil, star: 6)
        XCTAssertNil(largeStarMeal)
        
        
    }

}
