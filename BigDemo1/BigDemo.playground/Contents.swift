//: Playground - noun: a place where people can play

import UIKit
struct Date {
    var day: Int
    var months: Int
    var year: Int
    init(day: Int, months: Int, year: Int) {
        self.day = day
        self.months = months
        self.year = year
    }
    
    
}

struct Time {
    var seconds: Int
    var minutes: Int
    var hours: Int
    init(seconds: Int, minutes: Int, hours: Int) {
        self.seconds = seconds
        self.minutes = minutes
        self.hours = hours
    }
}
//account
class Account {
    var accountID: Int
    var userName: String
    var passWord: String
    var firstName: String?
    var lastName: String?
    var sex: String?
    var phone: Int
    var location: String?
    var address: String
    
    init(accountID: Int, userName: String, passWord: String, firstName: String?,
         lastName: String?, sex: String?, phone: Int, location: String?, address: String) {
        self.accountID = accountID
        self.userName = userName
        self.passWord = passWord
        self.sex = sex
        self.firstName = firstName
        self.lastName = lastName
        self.phone = phone
        self.location = location
        self.address = address
    }
}

class Place {
    var placeID: Int
    var cityID: Int
    var details: String
    var namePlace: String
    var views: Int
    var website: String?
    var phone: Int?
    var email: String?
    var fax: Int?
    var address: String
    var typeID: Int
    
    init(placeID: Int, cityID: Int, details: String, namePlace: String, views: Int, website: String?,
         phone: Int?, email: String?, fax: Int?, address: String, typeID: Int) {
        self.phone = phone
        self.placeID = placeID
        self.cityID = cityID
        self.namePlace = namePlace
        self.views = views
        self.details = details
        self.email = email
        self.address = address
        self.fax = fax
        self.typeID = typeID
        self.website = website
    }
}

class City {
    var cityID: Int
    var provinceID: Int?
    var name: String
    var country: String
    
    init(cityID: Int, provinceID: Int?, name: String, country: String) {
        self.cityID = cityID
        self.provinceID = provinceID
        self.name = name
        self.country = country
    }
}

class Province {
    var provinceID: Int
    var name: String
    var country: String
    
    init(provinceID: Int, name: String, country: String) {
        self.provinceID = provinceID
        self.name = name
        self.country = country
    }
}

class images {
    var imageURL: String
    var placeID: String
    var created: (Date,Time)
    var used: Bool
    
    init(imageURL: String, placeID: String, created: (Date,Time), used: Bool) {
        self.imageURL = imageURL
        self.placeID = placeID
        self.created = created
        self.used = used
    }
}

class PlaceType {
    var typeID: Int
    var name: String
    
    init(typeID: Int, name: String) {
        self.typeID = typeID
        self.name = name
    }
}

class Comment {
    var commentID: Int
    var accountID: Int
    var placeID: Int
    var stars: Int?
    var comment: String
    var dateTime: (Date, Time)
    var title: String
    
    init(commentID: Int, accountID: Int, placeID: Int, stars: Int?, comment: String,
         dateTime: (Date,Time), title: String) {
        self.commentID = commentID
        self.accountID = accountID
        self.placeID = placeID
        self.dateTime = dateTime
        self.title = title
        self.stars = stars
        self.comment = comment
    }
}

class Hotel {
    var hotelID: Int
    var placeID: Int
    var name: String
    var rooms: Int
    var roomAvaiable: Int
    
    init(hotelID: Int, placeID: Int, name: String, rooms: Int, roomAvaiable: Int) {
        self.hotelID = hotelID
        self.name = name
        self.placeID = placeID
        self.rooms = rooms
        self.roomAvaiable = roomAvaiable
    }
}

class Room {
    var roomID: Int
    var hotelID: Int
    var name: String
    var typeID: Int
    var status: String
    
    init(roomID: Int, hotelID: Int, name: String, typeID: Int, status: String) {
        self.hotelID = hotelID
        self.roomID = roomID
        self.name = name
        self.typeID = typeID
        self.status = status
    }
}

class RoomType {
    var roomTypeID: Int
    var name: String
    var detail: String
    var prices: Int
    
    init(roomTypeID: Int, name: String, detail: String, prices: Int) {
        self.roomTypeID = roomTypeID
        self.name = name
        self.detail = detail
        self.prices = prices
    }
}

class dateBooking {
    var date: (Date, Time)
    var bookRoomID: Int
    var roomID: Int
    
    init(date: (Date,Time), bookRoomID: Int, roomID: Int) {
        self.date = date
        self.bookRoomID = bookRoomID
        self.roomID = roomID
    }
}

class BookRoom {
    var bookRoomID: Int
    var accountID: Int
    var created: (Date,Time)
    var updated: (Date,Time)?
    var dateUpdate: Date?
    var startDate: Date
    var endDate: Date
    var discount: Int
    var prices: Int
    var note: String?
    
    init(bookRoomID: Int, accountID: Int, created: (Date, Time), updated: (Date, Time)?, dateUpdate: Date?,
         startDate: Date, endDate: Date, discount: Int, prices: Int, note: String?) {
        self.accountID = accountID
        self.bookRoomID = bookRoomID
        self.created = created
        self.updated = updated
        self.dateUpdate = dateUpdate
        self.discount = discount
        self.prices = prices
        self.startDate = startDate
        self.endDate = endDate
        self.note = note
        
    }
    
}

class BookTicket {
    var accountID: Int
    var ID: Int
    var created: (Date,Time)
    var updated: (Date,Time)?
    var dateUpdate: Date?
    var discount: Int?
    var price: Int
    var notes: String?
    
    init(accountID: Int, ID: Int, created: (Date,Time), updated: (Date,Time)?, dateUpdate: Date?,
         discount: Int?, price: Int, note: String?) {
        self.accountID = accountID
        self.ID = ID
        self.created = created
        self.discount = discount
        self.price = price
        self.updated = updated
        self.dateUpdate = dateUpdate
        self.notes = note
    }
}

class AirTicket {
    var ticketID: Int
    var bookTicketID: Int
    var flightID: Int
    var typeID: Int
    var dateReturn: (Date,Time)?
    var seats: String
    var discount: Int?
    var personID: Int
    var country: String
    var birthday: Date
    var phone: Int?
    var email: String?
    var sex: String
    var lastName: String
    var firstName: String
    
    init(ticketID: Int, bookTicketID: Int, flightID: Int, typeID: Int, dateReturn: (Date,Time)?, seats: String,
         discount: Int?, personID: Int, country: String, birthday: Date, phone: Int?, email: String?,
        sex: String, lastName: String, firstName: String) {
        self.ticketID = ticketID
        self.bookTicketID = bookTicketID
        self.flightID = flightID
        self.dateReturn = dateReturn
        self.sex = sex
        self.seats = seats
        self.discount = discount
        self.country = country
        self.personID = personID
        self.birthday = birthday
        self.firstName = firstName
        self.lastName = lastName
        self.typeID = typeID
        self.phone = phone
        self.email = email
    }
}

class Flight {
    var flightID: Int
    var airPortID: Int
    var dateTimeFly: (Date,Time)
    var dateTimeArrive: (Date,Time)
    var airTickets: Int
    var airLine: Int
    var airRow: Int
    var toPlace: String
    var dateUpdate: Date?
    
    init(flightID: Int, airPort: Int, dateTimeFly: (Date,Time), dateTimeArrive: (Date,Time), airTicket: Int,
         airLine: Int, toPlace: String, dateUpdate: Date?) {
        self.airPortID = airPortID
        self.flightID = flightID
        self.airLine = airLine
        self.airRow = airRow
        self.airTickets = airTickets
        self.dateTimeArrive = dateTimeArrive
        self.dateTimeFly = dateTimeFly
        self.dateUpdate = dateUpdate
        self.toPlace = toPlace
    }
    
}

class TypeTicket {
    var typeID: Int
    var type: String
    var price: Int
    var age: Int
    var oneWay: Bool
    var services: String
    
    init(typeID: Int, type: String, price: Int, age: Int, oneWay: Bool, services: String) {
        self.typeID = typeID
        self.type = type
        self.age = age
        self.price = price
        self.oneWay = oneWay
        self.services = services
    
    }
}

class Status {
    var flightID: Int
    var typeID: Int
    var status: String
    var amount: Int
    
    init(flightID: Int, typeID: Int, status: String, amount: Int) {
        self.flightID = flightID
        self.amount = amount
        self.typeID = typeID
        self.status = status
    }
}

class AirPort {
    var airPortID: Int
    var placeID: Int
    var name: String
    
    init(airPort: Int, placeID: Int, name: String) {
        self.airPortID = airPortID
        self.placeID = placeID
        self.name = name
    }
}
