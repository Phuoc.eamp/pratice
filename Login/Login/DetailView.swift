//
//  DetailView.swift
//  Login
//
//  Created by PhuocNguyen on 12/2/18.
//  Copyright © 2018 PhuocNguyen. All rights reserved.
//

import UIKit
class DetailViewController: UIViewController {
    var contact: Contact!
    var line: Int!
    @IBOutlet weak var phoneNumberLB: UILabel!
    @IBOutlet weak var nameLB: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        phoneNumberLB.text = contact?.phoneNumber
        nameLB.text = contact?.name
        print(line)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "EditContact"{
            let editContact = segue.destination as! AddContactViewController
            editContact.contact = contact
            editContact.line = self.line
        }
//        if segue.identifier == "DeleteContact"{
//            let deleteContact = segue.destination as! ContactTableViewController
//            deleteContact.contactLine.remove(at: self.line)
//        }
    }
}
