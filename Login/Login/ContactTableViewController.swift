//
//  ContactTableViewController.swift
//  Login
//
//  Created by PhuocNguyen on 12/6/18.
//  Copyright © 2018 PhuocNguyen. All rights reserved.
//

import UIKit
class ContactTableViewController: UITableViewController {
    
    //var section = [String: [Contact]]()
    
    var contactLine: [Contact] = [Contact(name: nil, phoneNumber: "012345623"),
                                  Contact(name: "c2", phoneNumber: "02918219"),
                                  Contact(name: "A1", phoneNumber: "91391283012"),
                                  Contact(name: "ch", phoneNumber: "2313123"),
                                  Contact(name: "B2", phoneNumber: "282147912"),
                                  Contact(name: "C2", phoneNumber: "293192")]
    var section = [String: [Contact]]()
    override func viewDidLoad() {
        super.viewDidLoad()
        getSection()
        tableView.reloadData()
        print(contactLine.count)
        //contactLine.sort()
        print("table view")
    }
    
    //    override   func viewWillAppear(_ animated: Bool) {
    //        if let contacts = contacts {
    //            contactLine.append(contacts)
    //            let indexInsert = IndexPath(row: contactLine.count - 1, section: 0)
    //            tableView.insertRows(at: [indexInsert], with: .automatic)
    //            tableView.reloadData()
    //        } else{
    //            print("Don't insert")
    //        }
    //
    //    }
    
    func getSection() {
        let array = contactLine.filter({$0.name != nil})
        array.forEach { (element: Contact) in
            if let character = element.name?.first{
                section[String(character).uppercased()] = array.filter({ (contact: Contact) -> Bool in
                    if let firstCharacter = contact.name?.first{
                        return String(firstCharacter).uppercased() == String(character).uppercased()
                    }
                    return false
                }).sorted()
            }
        }
        section["Number"] = contactLine.filter({$0.name == nil})
        
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return section.count
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return self.section.keys.sorted()[section]
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//                if section == self.section.count - 1 {
//                    let array = contactLine.filter({$0.name == nil})
//                    return array.count
//                } else {
//                    let text = self.section[section]
//                    let array = contactLine.filter({$0.name != nil})
//                    let arr = array.filter { (contact) -> Bool in
//                        guard let character = contact.name?.first else { return false }
//                        return String(character).uppercased() == text
//                    }
//                    return arr.count
//                }
        var array = [Int]()
        var dict = self.section.keys.sorted()
        self.section.forEach { (key: String, value: [Contact]) in
        guard let contact = self.section[dict[section]] else {return}
            array.append(contact.count)
        }
        return array[section]
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell")
        let dict = self.section.keys.sorted()
        if let contact = section[dict[indexPath.section]] {
        cell?.textLabel?.text = contact[indexPath.row].name ?? contact[indexPath.row].phoneNumber
        }
        return cell!
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            let DetailViewController = storyboard?.instantiateViewController(withIdentifier: "DetailViewController")
            if let DetailViewController = DetailViewController as? DetailViewController {
            let section = self.section.keys.sorted()[indexPath.section]
            if let sectionSelect = self.section[section] {
                DetailViewController.contact = sectionSelect[indexPath.row]
                //DetailViewController.indexPath = (indexPath.section,indexPath.row)
                var line = 0
                contactLine.enumerated().forEach { (offset: Int, element: Contact) in
                    if element == sectionSelect[indexPath.row]{
                        line = offset
                    }
                }
                DetailViewController.line = line
            }
            self.navigationController?.pushViewController(DetailViewController, animated: true)
        }
    }
    
    @IBAction func AddContact(segue: UIStoryboardSegue){
        print("Add")
        if let addContactViewController = segue.source as? AddContactViewController{
            if let line = addContactViewController.line{
                contactLine[line].name = addContactViewController.nameTextField.text
                contactLine[line].phoneNumber = addContactViewController.phoneNumberTextField.text
                getSection()
                tableView.reloadData()
            }else {
                if let contact = addContactViewController.contact{
                    self.contactLine.append(contact)
                    print(contactLine.index(after: contactLine.endIndex))
                    getSection()
                    tableView.reloadData()
                }
            }
        }
    }
}

