//
//  AddContactViewController.swift
//  Login
//
//  Created by PhuocNguyen on 12/3/18.
//  Copyright © 2018 PhuocNguyen. All rights reserved.
//

import UIKit

class AddContactViewController: UIViewController{
    
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var phoneNumberTextField: UITextField!
    var contact: Contact?
    var line: Int?
    override func viewDidLoad() {
        super.viewDidLoad()
        print("add view")
        nameTextField.placeholder = self.contact?.name
        phoneNumberTextField.placeholder = self.contact?.phoneNumber
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "AddContact"{
            if phoneNumberTextField.text != "" {
                self.contact = Contact(name: nameTextField.text, phoneNumber: phoneNumberTextField.text)
            }
        }
    }
    
}
