//
//  LoginViewController.swift
//  Login
//
//  Created by PhuocNguyen on 11/30/18.
//  Copyright © 2018 PhuocNguyen. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    @IBOutlet weak var txtUserName: UITextField!
    @IBOutlet weak var txtPassWord: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        //view.backgroundColor = UIColor.green
    }
    
    
    @IBAction func login(_ sender: Any) {
        let mainTab = storyboard?.instantiateViewController(withIdentifier: "mainTabController") as! MainTabController
        mainTab.selectedViewController = mainTab.viewControllers?[0]
        present(mainTab, animated: true, completion: nil)
    }
}
