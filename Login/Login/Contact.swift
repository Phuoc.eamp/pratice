//
//  Contact.swift
//  Login
//
//  Created by PhuocNguyen on 12/7/18.
//  Copyright © 2018 PhuocNguyen. All rights reserved.
//

import Foundation
class Contact: Comparable {
    
    var name: String?
    var phoneNumber: String!
    
    init(name: String?, phoneNumber: String!) {
        self.phoneNumber = phoneNumber
        self.name = name
    }
    
    static func == (lhs: Contact, rhs: Contact) -> Bool {
        guard let name1 = lhs.name, let name2 = rhs.name else {
            return lhs.phoneNumber == rhs.phoneNumber
        }
        return name1 == name2
        
    }
    
    static func < (lhs: Contact, rhs: Contact) -> Bool {
        guard let name1 = lhs.name, let name2 = rhs.name else {
            return lhs.phoneNumber < rhs.phoneNumber
        }
        return name1 < name2
        
    }
}
