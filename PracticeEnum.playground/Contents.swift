//: Playground - noun: a place where people can play

import UIKit

print("----------Default enum")
enum Direction {
    case North
    case West
    case East
    case South
}
var dir = Direction.South

switch dir {
case .East:
    print("East")
case .North:
    print("North")
case .West:
    print("West")
case .South:
    print("South")
}

func direction(direction: Direction) {
    switch direction {
    case .East:
        print("East")
    case .North:
        print("North")
    case .West:
        print("West")
    case .South:
        print("South")
    }
}

direction(direction: Direction.North)

print("----------value enum and computed properties")

enum Boss: Int {
    case level1 = 1
    case level2
    case level3
    
    var name: String {
        switch self {
        case .level1:
            return "Doraemon"
        case .level2:
            return "Nobita"
        case .level3:
            return "Xuka"
        }
    }
    
    var mess: String {
        return "Welcome to level \(rawValue)"
    }
}

var boss = Boss(rawValue: 2)
print(boss!.mess)

print("----------func in enum")

extension Boss {
    func killBoss() {
        // do some thing to kill boss
    }
    mutating func nextLevel() {
        switch self {
        case .level1:
            self = .level2
            print(self.mess)
        case .level2:
            self = .level3
            print(self.mess)
        case .level3:
            print("Game over!!!")
        }
    }
}

boss?.nextLevel()
boss?.nextLevel()

print("----------nested enum")

enum Character {
    enum weapon: Int {
        case sword = 3000
        case crow = 1000
        case axe = 900
        case guns = 6000
        var cost: String {
            return "\(self): buy \(rawValue) and sell \(rawValue)"
        }
        
    }
    enum helmet: Int {
        case iron = 1000
        case wood = 500
        case gold = 3000
        var cost: String {
            return "\(self): buy \(rawValue) and sell \(rawValue)"
        }
    }
    case Thief
    case Warrior
    case Knight
}

var character = Character.Knight
var helmet = Character.helmet.gold
var weapon = Character.weapon.sword

print(helmet.cost)

print("----------indirect enum")

indirect enum Caculation {
    case number (Int)
    case add (Caculation, Caculation)
    case multipli (Caculation, Caculation)
    func caculation() -> Int {
        switch self {
        case .add(let a, let b):
            return a.caculation() + b.caculation()
        case .multipli(let a, let b):
            return a.caculation() * b.caculation()
        case .number(let a):
            return a
        }
    }
}

var a = Caculation.number(5)
var b = Caculation.number(6)
var add = Caculation.add(a, b)
add.caculation()
var multi = Caculation.multipli(add, a)
multi.caculation()
multi = Caculation.multipli(multi, multi)
multi.caculation()
// use enum to compare 
enum Equal {
    case rectangle (weight: Int, height: Int)
    case triangle (line1: Int, line2: Int, line3: Int)
    case cycle (radius: Int)
    func compare(by item: Equal) -> Bool{
        switch (self, item) {
        case let (.rectangle(weight: a, height: b), .rectangle(weight: c, height: d)):
            return a == c && b == d
        case let (.triangle(line1: a, line2: b, line3: c), .triangle(line1: x, line2: y, line3: z)):
            return a == x && b == y && c == z
        case let (.cycle(radius: a),.cycle(radius: b)):
            return a == b
        default:
            print("This isn't same type!!")
            return false
        }
    }
}

var cycle1 = Equal.cycle(radius: 5)
var cycle2 = Equal.cycle(radius: 5)
var rectangle1 = Equal.rectangle(weight: 5, height: 6)
cycle1.compare(by: cycle2)



