//: Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"



func selectionSort<T: Comparable>(array: inout[T]) {
    var min = 0
    for i in 0..<array.count - 1 {
        min = i
        for j in i+1..<array.count where array[j] < array[min] {
            min = j
        }
        let temp = array[i]
        array[i] = array[min]
        array[min] = temp
//      swap(&array[i], &array[min])
    }
}
func selectionNilSorted<T: Comparable>(array: inout[T]) {
    
}
var arr = ["a","c","e","d","b","a","c"]

selectionSort(array: &arr)

func insertionSort<T: Comparable>(array: inout[T]) {
    var max: T
    var j = 0
    for i in 1..<array.count {
        max = array[i]
        j = i - 1
        while j >= 0 && array[j] > max {
            array[j+1] = array[j]
            j -= 1
        }
        array[j+1] = max
    }
}

insertionSort(array: &arr)


func bubbleSorted<T: Comparable>(array: inout [T]) {
    for i in 0..<array.count - 1 {
        for j in 0..<array.count - i - 1 where array[j + 1] < array[j] {
//            array.swapAt(i, j)
            let temp = array[j + 1]
            array[j + 1] = array[j]
            array[j] = temp
        }
    }
}

bubbleSorted(array: &arr)
var arrA = [6,2,3,0,1]
arrA.sorted { (a,b) -> Bool in
    print("a = \(a)")
    print("b = \(b)")
    print(a < b)
    return a < b
}

func swap<T: Comparable>(_ a: inout T, _ b: inout T) {
    let temp = a
    a = b
    b = temp
}

func merge<T: Comparable>(lhs: [T], rhs: [T]) -> [T]{
    var lhsIndex = 0
    var rhsIndex = 0
    var arr = [T]()
    while lhsIndex < lhs.count && rhsIndex < rhs.count {
        if lhs[lhsIndex] < rhs[rhsIndex] {
            arr.append(lhs[lhsIndex])
            lhsIndex += 1
        } else if lhs[lhsIndex] > rhs[rhsIndex] {
            arr.append(rhs[rhsIndex])
            rhsIndex += 1
        } else {
            arr += [lhs[lhsIndex],rhs[rhsIndex]]
            rhsIndex += 1
            lhsIndex += 1
        }
    }
//    if lhsIndex < lhs.count {
//        arr += lhs[lhsIndex..<lhs.endIndex]
//    }
    arr += lhsIndex < lhs.count ? lhs[lhsIndex..<lhs.endIndex] : rhs[rhsIndex..<rhs.endIndex]
    return arr
}

func mergeSort<T: Comparable>(array: [T]) -> [T] {
    guard array.count > 1 else {
        return array
    }
    let middleIndex = array.count / 2
    let leftArray = mergeSort(array: Array(array[0..<middleIndex]))
    let rightArray = mergeSort(array: Array(array[middleIndex..<array.count]))
    return merge(lhs: leftArray, rhs: rightArray)
}

var a = [2,7,10]
var b = [1,6,8]
merge(lhs: a, rhs: b)

mergeSort(array: arr)

func quicksort<T: Comparable>(_ array: [T]) -> [T] {
    guard array.count > 1 else { return array }
    
    let origin = array[array.startIndex]
    let less = array.filter { $0 < origin }
    let equal = array.filter { $0 == origin }
    let greater = array.filter { $0 > origin }
    return quicksort(less) + equal + quicksort(greater)
}

quicksort(arr)
